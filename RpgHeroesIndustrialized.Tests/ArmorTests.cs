﻿using RpgHeroesIndustrialized.Items;
using RpgHeroesIndustrialized.Enums;
using RpgHeroesIndustrialized.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Tests
{
    public class ArmorTests
    {
        [Fact]
        public void Constructor_Name()
        {
            //Arrange
            string name = "Test";
            string expected = name;

            //Act
            IItem armor = new Armor(name, 0, ItemSlot.BODY, ArmorType.MAIL);
            string actual = armor.Name;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_RequiredLevel()
        {
            //Arrange
            int requiredLevel = 3;
            int expected = requiredLevel;

            //Act
            IItem armor = new Armor("Test", requiredLevel, ItemSlot.BODY, ArmorType.MAIL);
            int actual = armor.RequiredLevel;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_ItemSlot()
        {
            //Arrange
            var slot = ItemSlot.LEGS;
            var expected = slot;

            //Act
            var armor = new Armor("Test", 0, slot, ArmorType.CLOTH);
            var actual = armor.Slot;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Attribute_AttributeNone()
        {
            //Arrange & Act
            var armor = new Armor("Test", 0, ItemSlot.HEAD, ArmorType.CLOTH);
            var actual = armor.ArmorAttribute;

            //Assert
            Assert.Null(actual);
        }

        [Fact]
        public void Attribute_WithAttribute()
        {
            //Arrange
            IAttribute attribute = new HeroAttribute(1, 1, 1);
            var expected = attribute;

            //Act
            var armor = new Armor("Test", 0, ItemSlot.HEAD, ArmorType.CLOTH, attribute);
            var actual = armor.ArmorAttribute;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_ArmorType()
        {
            //Arrange
            var armorType = ArmorType.MAIL;
            var expected = armorType;

            //Act
            var armor = new Armor("Test", 0, ItemSlot.HEAD, armorType);
            var actual = armor.Type;

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
