﻿using RpgHeroesIndustrialized.Data;
using RpgHeroesIndustrialized.Heroes;
using RpgHeroesIndustrialized.Items;
using RpgHeroesIndustrialized.Enums;
using RpgHeroesIndustrialized.Extensions;
using RpgHeroesIndustrialized.Attributes;

namespace RpgHeroesIndustrialized.Tests
{
    public class HeroTests
    {
        [Theory]
        [InlineData(HeroClass.MAGE)]
        [InlineData(HeroClass.RANGER)]
        [InlineData(HeroClass.ROGUE)]
        [InlineData(HeroClass.WARRIOR)]
        public void Constructor_ValidWeaponTypes(HeroClass classType)
        {
            //Arrange
            IHeroFactory heroFactory = new HeroFactory();
            IHero hero = heroFactory.GetHero("Test", classType);

            List<IWeapon?> weapons = new();
            GameData.ClassData[classType].ValidWeaponTypes.ForEach(weaponType =>
            {
                weapons.Add(new Weapon("Weapon", 0, 0, weaponType));
            });

            //Act & Assert
            weapons.ForEach(w => {
                IWeapon? expected = w;

                hero.Equip(w!);
                IWeapon? actual = hero.Equipment.Weapon;

                Assert.Equal(expected, actual);
            });
        }

        [Theory]
        [InlineData(HeroClass.MAGE)]
        [InlineData(HeroClass.RANGER)]
        [InlineData(HeroClass.ROGUE)]
        [InlineData(HeroClass.WARRIOR)]
        public void Constructor_BaseAttributes(HeroClass classType)
        {
            //Arrange
            IHeroFactory heroFactory = new HeroFactory();
            IHero hero = heroFactory.GetHero("Test", classType);
            IAttribute expected = GameData.ClassData[classType].BaseAttributes;

            //Act
            IAttribute actual = hero.LevelAttribute;

            //Assert
            Assert.Equal(expected as HeroAttribute, actual as HeroAttribute);
        }

        [Fact]
        public void Constructor_Name()
        {
            //Arrange
            string name = "Hero";
            string expected = name;

            //Act
            IHero hero = new Hero(name, HeroClass.MAGE);
            string actual = hero.Name;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalAttribute_NoArmor()
        {
            //Arrange
            IHero hero = new Hero("Hero of Testing", HeroClass.MAGE);
            IAttribute expected = GameData.ClassData[HeroClass.MAGE].BaseAttributes;

            //Act
            IAttribute actual = hero.TotalAttribute();

            //Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact]
        public void TotalAttribute_SomeArmor()
        {
            //Arrange
            IAttribute armorBonus = new HeroAttribute(1, 1, 1);
            IItem armor = new Armor("Cuirass of Testing", 0, ItemSlot.BODY, ArmorType.CLOTH, armorBonus);

            IHero hero = new Hero("Hero of Testing", HeroClass.MAGE);
            hero.Equip(armor);

            IAttribute expected = GameData.ClassData[HeroClass.MAGE].BaseAttributes.Add(armorBonus);

            //Act
            IAttribute actual = hero.TotalAttribute();

            //Assert
            Assert.Equal(expected as HeroAttribute, actual as HeroAttribute);
        }

        [Fact]
        public void TotalAttribute_FullArmor()
        {
            //Arrange
            IAttribute armorBonus = new HeroAttribute(1, 1, 1);
            IItem head = new Armor("Helmet of Testing", 0, ItemSlot.HEAD, ArmorType.MAIL, armorBonus);
            IItem body = new Armor("Cuirass of Testing", 0, ItemSlot.BODY, ArmorType.MAIL, armorBonus);
            IItem legs = new Armor("Greaves of Testing", 0, ItemSlot.LEGS, ArmorType.MAIL, armorBonus);

            IHero hero = new Hero("Hero of Testing", HeroClass.WARRIOR);
            hero.Equip(head);
            hero.Equip(body);
            hero.Equip(legs);

            List<IArmor?> armor = new() { (IArmor)head, (IArmor)body, (IArmor)legs };
            IAttribute expected = GameData.ClassData[HeroClass.WARRIOR].BaseAttributes + armor.AttributeSum();

            //Act
            IAttribute actual = hero.TotalAttribute();

            //Assert
            Assert.Equal(expected as HeroAttribute, actual as HeroAttribute);
        }

        [Fact]
        public void TotalAttribute_ReplacedArmor()
        {
            //Arrange
            IAttribute armorBonus = new HeroAttribute(1, 1, 1);
            IAttribute replaceArmorBonus = new HeroAttribute(2, 2, 2);
            IArmor head = new Armor("Helmet of Testing", 0, ItemSlot.HEAD, ArmorType.MAIL, armorBonus);
            IArmor replaceHead = new Armor("Cuirass of Testing", 0, ItemSlot.HEAD, ArmorType.MAIL, replaceArmorBonus);

            IHero hero = new Hero("Hero of Testing", HeroClass.WARRIOR);
            hero.Equip(head);
            hero.Equip(replaceHead);

            IAttribute expected = GameData.ClassData[HeroClass.WARRIOR].BaseAttributes + ((HeroAttribute) replaceHead.ArmorAttribute!);

            //Act
            IAttribute actual = hero.TotalAttribute();

            //Assert
            Assert.Equal(expected as HeroAttribute, actual as HeroAttribute);
        }

        [Fact]
        public void LevelUp_IncrementLevel()
        {
            //Arrange
            IHero hero = new Hero("Hero of Testing", HeroClass.MAGE);
            int expected = hero.Level + 1;

            //Act
            hero.LevelUp();
            int actual = hero.Level;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_IncreaseAttributes()
        {
            //Arrange
            ClassStruct data = GameData.ClassData[HeroClass.MAGE];
            IHero hero = new Hero("Hero of Testing", HeroClass.MAGE);
            IAttribute expected = data.BaseAttributes + data.LevelUpAttributes;

            //Act
            hero.LevelUp();
            IAttribute actual = hero.LevelAttribute;

            //Assert
            Assert.Equal(expected as HeroAttribute, actual as HeroAttribute);
        }

        [Fact]
        public void Damage_NoWeapon()
        {
            //Arrange
            IHero hero = new Hero("Hero", HeroClass.MAGE);
            
            ClassStruct data = GameData.ClassData[HeroClass.MAGE];
            double damageAttribute = data.BaseAttributes.GetAttribute(data.DamageAttribute);

            double unarmedDamage = 1;
            double expected = unarmedDamage * (1 + damageAttribute / 100);

            //Act
            double actual = hero.Damage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_WithWeapon()
        {
            //Arrange
            int damage = 5;

            IHero hero = new Hero("Hero", HeroClass.MAGE);
            IWeapon weapon = new Weapon("Staff", 0, damage, WeaponType.STAFF);
            hero.Equip(weapon);

            ClassStruct data = GameData.ClassData[HeroClass.MAGE];
            double damageAttribute = data.BaseAttributes.GetAttribute(data.DamageAttribute);
            double expected = damage * (1 + damageAttribute / 100);

            //Act
            double actual = hero.Damage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_WithWeaponAndLevel()
        {
            //Arrange
            int damage = 5;

            IHero hero = new Hero("Hero", HeroClass.MAGE);
            IWeapon weapon = new Weapon("Staff", 0, damage, WeaponType.STAFF);

            hero.LevelUp();
            hero.Equip(weapon);

            ClassStruct data = GameData.ClassData[HeroClass.MAGE];
            double damageAttribute = (data.BaseAttributes + data.LevelUpAttributes).GetAttribute(data.DamageAttribute);
            double expected = damage * (1 + damageAttribute / 100);

            //Act
            double actual = hero.Damage();

            //Assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void Damage_WithReplacedWeapon()
        {
            //Arrange
            int damage = 5;
            int replaceDamage = 10;

            IHero hero = new Hero("Hero", HeroClass.MAGE);

            IWeapon weapon = new Weapon("Staff", 0, damage, WeaponType.STAFF);
            IWeapon replaceWeapon = new Weapon("Staff", 0, replaceDamage, WeaponType.STAFF);

            hero.Equip(weapon);
            hero.Equip(replaceWeapon);

            ClassStruct data = GameData.ClassData[HeroClass.MAGE];
            double damageAttribute = data.BaseAttributes.GetAttribute(data.DamageAttribute);
            double expected = replaceDamage * (1 + damageAttribute / 100);

            //Act
            double actual = hero.Damage();

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
