﻿using RpgHeroesIndustrialized.Enums;
using RpgHeroesIndustrialized.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Tests
{
    public class WeaponTests
    {
        [Fact]
        public void Constructor_Name()
        {
            //Arrange
            string name = "Test";
            string expected = name;

            //Act
            IItem weapon = new Weapon(name, 0, 1, WeaponType.STAFF);
            string actual = weapon.Name;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_RequiredLevel()
        {
            //Arrange
            int requiredLevel = 3;
            int expected = requiredLevel;

            //Act
            IItem weapon = new Weapon("Test", requiredLevel, 1, WeaponType.STAFF);
            int actual = weapon.RequiredLevel;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_ItemSlot()
        {
            //Arrange
            var expected = ItemSlot.WEAPON;

            //Act
            var weapon = new Weapon("Test", 0, 1, WeaponType.STAFF);
            var actual = weapon.Slot;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_WeaponType()
        {
            //Arrange
            var weaponType = WeaponType.AXE;
            var expected = weaponType;

            //Act
            var weapon = new Weapon("Test", 0, 1, weaponType);
            var actual = weapon.Type;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_Damage()
        {
            //Arrange
            int damage = 10;
            int expected = damage;

            //Act
            var weapon = new Weapon("Test", 0, damage, WeaponType.STAFF);
            int actual = weapon.Damage;

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
