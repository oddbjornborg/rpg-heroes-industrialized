﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RpgHeroesIndustrialized.Attributes;
using RpgHeroesIndustrialized.Enums;
using RpgHeroesIndustrialized.Extensions;
using RpgHeroesIndustrialized.Items;

namespace RpgHeroesIndustrialized.Tests
{
    public class AttributeTests
    {
        [Fact]
        public void Constructor_AttributeValues()
        {
            //Arrange
            int strength = 1;
            int dexterity = 2;
            int intelligence = 3;

            //Act
            HeroAttribute heroAttribute = new(strength, dexterity, intelligence);

            //Assert
            Assert.Equal(strength, heroAttribute.Strength);
            Assert.Equal(dexterity, heroAttribute.Dexterity);
            Assert.Equal(intelligence, heroAttribute.Intelligence);
        }

        [Fact]
        public void Constructor_ZeroInitialize()
        {
            // Arrange
            HeroAttribute expected = new(0,0,0);

            //Act
            HeroAttribute actual = new();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_PositiveAndPositive()
        {
            //Arrange
            HeroAttribute a1 = new(1, 1, 1);
            HeroAttribute a2 = new(1, 1, 1);
            HeroAttribute expected = new(a1.Strength + a2.Strength, a1.Dexterity + a2.Dexterity, a1.Intelligence + a2.Intelligence);

            //Act
            var actual = a1 + a2;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_PositiveAndZero()
        {
            //Arrange
            HeroAttribute a1 = new(1, 1, 1);
            HeroAttribute a2 = new(0, 0, 0);
            var expected = a1;

            //Act
            var actual = a1 + a2;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_PositiveAndNegative()
        {
            //Arrange
            HeroAttribute a1 = new(2, 2, 2);
            HeroAttribute a2 = new(-1, -1, -1);
            HeroAttribute expected = new(a1.Strength + a2.Strength, a1.Dexterity + a2.Dexterity, a1.Intelligence + a2.Intelligence); ;

            //Act
            var actual = a1 + a2;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Sum_IAttribute()
        {
            //Assert
            IEquipment equipment = new Equipment(new() { ArmorType.CLOTH }, new());
            IItem item1 = new Armor("t", 0, ItemSlot.HEAD, ArmorType.CLOTH, new HeroAttribute(1, 1, 1));
            IItem item2 = new Armor("t", 0, ItemSlot.BODY, ArmorType.CLOTH, new HeroAttribute(1, 1, 1));
            IItem item3 = new Armor("t", 0, ItemSlot.LEGS, ArmorType.CLOTH, new HeroAttribute(1, 1, 1));
            equipment.Equip(item1, 1);
            equipment.Equip(item2, 1);
            equipment.Equip(item3, 1);
            IAttribute? expected = new HeroAttribute(3, 3, 3);

            IAttribute? actual = equipment.Armor.AttributeSumNew();

            Assert.Equal(actual as HeroAttribute, expected as HeroAttribute);
        }
    }
}
