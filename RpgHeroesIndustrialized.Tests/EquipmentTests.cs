namespace RpgHeroesIndustrialized.Tests
{
    using RpgHeroesIndustrialized.Enums;
    using RpgHeroesIndustrialized.Items;
    using RpgHeroesIndustrialized.Exceptions;
    using RpgHeroesIndustrialized.Extensions;
    using RpgHeroesIndustrialized.Attributes;

    public class EquipmentTests
    {
        [Fact]
        public void EquipWeapon_ValidWeapon_ShouldReturnWeapon()
        {
            //Arrange
            List<WeaponType> weaponTypes = new() { WeaponType.STAFF };
            IEquipment equipment = new Equipment(new List<ArmorType>(), weaponTypes);

            int level = 1;
            int requiredLevel = 0;
            IItem weapon = new Weapon("Staff of Testing", requiredLevel, 5, WeaponType.STAFF);
            IItem? expected = weapon;

            //Act
            equipment.Equip(weapon, level);
            IItem? actual = equipment.GetItem(ItemSlot.WEAPON);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipWeapon_InvalidType_ShouldThrowException()
        {
            //Arrange
            List<WeaponType> weaponTypes = new() { WeaponType.AXE };
            IEquipment equipment = new Equipment(new List<ArmorType>(), weaponTypes);

            int level = 1;
            int requiredLevel = 0;
            IItem weapon = new Weapon("Staff of Testing", requiredLevel, 5, WeaponType.STAFF);

            // Act & Assert
            Exception e = Assert.Throws<InvalidWeaponException>(() => equipment.Equip(weapon, level));
        }

        [Fact]
        public void EquipWeapon_InvalidLevel_ShouldThrowException()
        {
            //Arrange
            List<WeaponType> weaponTypes = new() { WeaponType.STAFF };
            IEquipment equipment = new Equipment(new List<ArmorType>(), weaponTypes);

            int level = 1;
            int requiredLevel = 2;
            IItem weapon = new Weapon("Staff of Testing", requiredLevel, 5, WeaponType.STAFF);

            // Act & Assert
            Exception e = Assert.Throws<InvalidWeaponException>(() => equipment.Equip(weapon, level));
        }

        [Fact]
        public void EquipArmor_ValidArmor_ShouldReturnArmor()
        {
            //Arrange
            List<ArmorType> armorTypes = new() { ArmorType.MAIL };
            IEquipment equipment = new Equipment(armorTypes, new List<WeaponType>());
            
            int level = 1;
            int requiredLevel = 0;
            IItem armor = new Armor("Cuirass of Testing", requiredLevel, ItemSlot.BODY, ArmorType.MAIL);
            IItem? expected = armor;

            //Act
            equipment.Equip(armor, level);
            IItem? actual = equipment.GetItem(ItemSlot.BODY);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_InvalidType_ShouldThrowException()
        {
            //Arrange
            List<ArmorType> armorTypes = new() { ArmorType.CLOTH };
            IEquipment equipment = new Equipment(armorTypes, new List<WeaponType>());

            int level = 1;
            int requiredLevel = 0;
            IItem armor = new Armor("Cuirass of Testing", requiredLevel, ItemSlot.BODY, ArmorType.MAIL);

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => equipment.Equip(armor, level));
        }

        [Fact]
        public void EquipArmor_InvalidLevel_ShouldThrowException()
        {
            //Arrange
            List<ArmorType> armorTypes = new() { ArmorType.MAIL };
            IEquipment equipment = new Equipment(armorTypes, new List<WeaponType>());
            
            int level = 1;
            int requiredLevel = 2;
            IItem armor = new Armor("Cuirass of Testing", requiredLevel, ItemSlot.BODY, ArmorType.MAIL);

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => equipment.Equip(armor, level));
        }

        [Fact]
        public void GetArmor_ShouldReturnList()
        {
            //Arrange
            List<IItem> armor = new()
            {
                new Armor("Helmet of Testing", 0, ItemSlot.HEAD,  ArmorType.CLOTH),
                new Armor("Cuirass of Testing", 0, ItemSlot.BODY,  ArmorType.CLOTH),
                new Armor("Greaves of Testing", 0, ItemSlot.LEGS, ArmorType.CLOTH)
            };

            List<ArmorType> armorTypes = new() { ArmorType.CLOTH };
            IEquipment equipment = new Equipment(armorTypes, new List<WeaponType>());
            armor.ForEach(a => equipment.Equip(a, 1));

            List<IArmor?> expected = armor.Select(a => (IArmor?)a).ToList();

            //Act
            List<IArmor?> actual = equipment.Armor;

            //Assert
            Assert.True(expected.SequenceEqual(actual));
        }

        [Fact]
        public void Extension_AttributeSum()
        {
            //Arrange
            HeroAttribute headBonus = new(1, 1, 1);
            HeroAttribute bodyBonus = new(1, 1, 1);
            HeroAttribute legsBonus = new(1, 1, 1);

            List<Armor?> armor = new()
            {
                new Armor("Helmet of Testing", 0, ItemSlot.HEAD,  ArmorType.CLOTH, headBonus),
                new Armor("Cuirass of Testing", 0, ItemSlot.BODY,  ArmorType.CLOTH, bodyBonus),
                new Armor("Greaves of Testing", 0, ItemSlot.LEGS, ArmorType.CLOTH, legsBonus)
            };

            IAttribute expected = headBonus + bodyBonus + legsBonus;

            //Act
            IAttribute actual = armor.AttributeSum();

            //Assert
            Assert.Equal(expected as HeroAttribute, actual as HeroAttribute);
        }
    }
}