﻿using RpgHeroesIndustrialized.Attributes;
using RpgHeroesIndustrialized.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Items
{
    public interface IArmor : IItem
    {
        public IAttribute? ArmorAttribute { get; }
        public ArmorType Type { get; }
    }
}
