﻿using RpgHeroesIndustrialized.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Items
{
    public interface IItem
    {
        public string Name { get; }
        public int RequiredLevel { get; }
        public ItemSlot Slot { get; }
    }
}
