﻿using RpgHeroesIndustrialized.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Items
{
    public class Weapon : IWeapon
    {
        private readonly string _name;
        private readonly int _requiredLevel;
        private readonly ItemSlot _slot;
        private readonly int _damage;
        private readonly WeaponType _type;

        public string Name => _name;
        public int RequiredLevel => _requiredLevel;
        public ItemSlot Slot => _slot;
        public int Damage => _damage;
        public WeaponType Type => _type;

        public Weapon(string name, int requiredLevel, int damage, WeaponType type)
        {
            _damage = damage;
            _type = type;
            _name = name;
            _requiredLevel = requiredLevel;
            _slot = ItemSlot.WEAPON;
        }
    }
}
