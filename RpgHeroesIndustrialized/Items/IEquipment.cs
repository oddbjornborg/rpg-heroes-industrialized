﻿using RpgHeroesIndustrialized.Attributes;
using RpgHeroesIndustrialized.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Items
{
    public interface IEquipment
    {
        public List<IArmor?> Armor { get; }
        public IWeapon? Weapon { get; }

        public void Equip(IItem item, int level);
        public IItem? GetItem(ItemSlot item);
        public IAttribute? TotalArmorAttribute();
    }
}
