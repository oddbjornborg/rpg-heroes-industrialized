﻿using RpgHeroesIndustrialized.Attributes;
using RpgHeroesIndustrialized.Enums;
using RpgHeroesIndustrialized.Exceptions;
using RpgHeroesIndustrialized.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Items
{
    public class Equipment : IEquipment
    {
        private readonly List<ArmorType> _validArmorTypes;
        private readonly List<WeaponType> _validWeaponTypes;
        private readonly IDictionary<ItemSlot, IItem?> _items;

        public List<IArmor?> Armor => _items.Where(i => i.Key != ItemSlot.WEAPON).Select(i => (IArmor?)i.Value).ToList();
        public IWeapon? Weapon => (IWeapon?)_items[ItemSlot.WEAPON];

        public Equipment(List<ArmorType> validArmorTypes, List<WeaponType> validWeaponTypes)
        {
            _validArmorTypes = validArmorTypes;
            _validWeaponTypes = validWeaponTypes;

            _items = new Dictionary<ItemSlot, IItem?>() {
                { ItemSlot.WEAPON, null },
                { ItemSlot.HEAD, null },
                { ItemSlot.BODY, null },
                { ItemSlot.LEGS, null }
            };
        }

        public void Equip(IItem item, int level)
        {
            if (item.Slot == ItemSlot.WEAPON)
            {
                if (!_validWeaponTypes.Contains(((Weapon)item).Type))
                    throw new InvalidWeaponException("Weapon is not equippable by class");
                else if (level < item.RequiredLevel)
                    throw new InvalidWeaponException("Level too low");
            }
            else
            {
                if (!_validArmorTypes.Contains(((Armor)item).Type))
                    throw new InvalidArmorException("Armor is not equippable by class");
                else if (level < item.RequiredLevel)
                    throw new InvalidArmorException("Level too low");
            }

            _items[item.Slot] = item;
        }

        public IItem? GetItem(ItemSlot slot) => _items[slot];

        public IAttribute? TotalArmorAttribute()
        {
            return Armor.AttributeSumNew();
        }
    }
}
