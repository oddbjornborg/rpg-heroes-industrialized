﻿using RpgHeroesIndustrialized.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Items
{
    public interface IWeapon : IItem
    {
        public int Damage { get; }
        public WeaponType Type { get; }
    }
}
