﻿using RpgHeroesIndustrialized.Attributes;
using RpgHeroesIndustrialized.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Items
{
    public class Armor : IArmor
    {
        private readonly string _name;
        private readonly int _requiredLevel;
        private readonly ItemSlot _slot;
        private readonly IAttribute? _armorAttribute;
        private readonly ArmorType _type;

        public string Name => _name;
        public int RequiredLevel => _requiredLevel;
        public ItemSlot Slot => _slot;
        public IAttribute? ArmorAttribute => _armorAttribute;
        public ArmorType Type => _type;

        public Armor(string name, int requiredLevel, ItemSlot slot, ArmorType type, IAttribute armorAttribute)
        {
            _name = name;
            _requiredLevel = requiredLevel;
            _slot = slot;
            _armorAttribute = armorAttribute;
            _type = type;
        }

        public Armor(string name, int requiredLevel, ItemSlot slot, ArmorType type)
        {
            _name = name;
            _requiredLevel = requiredLevel;
            _slot = slot;
            _type = type;
        }
    }
}
