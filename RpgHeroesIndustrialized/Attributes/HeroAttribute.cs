﻿using RpgHeroesIndustrialized.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Attributes
{
    public class HeroAttribute : IAttribute
    {
        private readonly IDictionary<AttributeType, int> _attributes;

        public int Strength { get => _attributes[AttributeType.STRENGTH]; }
        public int Dexterity { get => _attributes[AttributeType.DEXTERITY]; }
        public int Intelligence { get => _attributes[AttributeType.INTELLIGENCE]; }

        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            _attributes = new Dictionary<AttributeType, int>()
            {
                {AttributeType.STRENGTH, strength},
                {AttributeType.DEXTERITY, dexterity},
                {AttributeType.INTELLIGENCE, intelligence}
            };
        }

        public HeroAttribute()
        {
            _attributes = new Dictionary<AttributeType, int>()
            {
                {AttributeType.STRENGTH, 0},
                {AttributeType.DEXTERITY, 0},
                {AttributeType.INTELLIGENCE, 0}
            };
        }

        public static HeroAttribute operator +(HeroAttribute a, HeroAttribute b)
        {
            return new HeroAttribute(
                a.Strength + b.Strength,
                a.Dexterity + b.Dexterity,
                a.Intelligence + b.Intelligence);
        }

        public IAttribute Add(IAttribute toAdd)
        {
            return this + (HeroAttribute)toAdd;
        }

        public int GetAttribute(AttributeType attribute) => _attributes[attribute];

        public override bool Equals(object? obj)
        {
            if (obj == null || !GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                HeroAttribute h = (HeroAttribute)obj;
                return _attributes.SequenceEqual(h._attributes);
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
