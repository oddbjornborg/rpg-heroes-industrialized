﻿using RpgHeroesIndustrialized.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Attributes
{
    public interface IAttribute
    {
        public int Strength { get; }
        public int Dexterity { get; }
        public int Intelligence { get; }

        public IAttribute Add(IAttribute toAdd);
        public int GetAttribute(AttributeType attributeType);
    }
}
