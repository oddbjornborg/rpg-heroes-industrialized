﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Enums
{
    public enum WeaponType
    {
        STAFF,
        WAND,
        BOW,
        DAGGER,
        SWORD,
        HAMMER,
        AXE
    }
}
