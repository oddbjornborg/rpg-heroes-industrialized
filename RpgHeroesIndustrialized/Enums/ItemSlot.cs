﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Enums
{
    public enum ItemSlot
    {
        WEAPON,
        HEAD,
        BODY,
        LEGS
    }
}
