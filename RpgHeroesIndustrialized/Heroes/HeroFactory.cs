﻿using RpgHeroesIndustrialized.Data;
using RpgHeroesIndustrialized.Enums;
using RpgHeroesIndustrialized.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Heroes
{
    public class HeroFactory : IHeroFactory
    {
        public IHero GetHero(string name, HeroClass heroClass)
        {
            ClassStruct data = GameData.ClassData[heroClass];

            IEquipment equipment = new Equipment(data.ValidArmorTypes, data.ValidWeaponTypes);

            IHero hero = new Hero(name, heroClass, equipment, data.BaseAttributes, data.LevelUpAttributes);

            return hero;
        }
    }
}
