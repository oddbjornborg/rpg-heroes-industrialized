﻿using RpgHeroesIndustrialized.Items;
using RpgHeroesIndustrialized.Enums;
using RpgHeroesIndustrialized.Extensions;
using RpgHeroesIndustrialized.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RpgHeroesIndustrialized.Attributes;

namespace RpgHeroesIndustrialized.Heroes
{
    public class Hero : IHero
    {
        private readonly string _name;
        private int _level;
        private readonly HeroClass _class; 
        private readonly IEquipment _equipment;
        private IAttribute _levelAttribute;
        private readonly IAttribute _levelUpAttribute;

        public string Name => _name;
        public int Level => _level;
        public IAttribute LevelAttribute => _levelAttribute;
        public IEquipment Equipment => _equipment;

        public Hero(string name, HeroClass classType)
        {
            _name = name;
            _level = 1;
            _class = classType;

            ClassStruct data = GameData.ClassData[_class];
            _equipment = new Equipment(data.ValidArmorTypes, data.ValidWeaponTypes);
            _levelAttribute = data.BaseAttributes;
            _levelUpAttribute = data.LevelUpAttributes;
        }

        public Hero(string name, HeroClass classType, IEquipment equipment, IAttribute baseAttribute, IAttribute levelUpBonus)
        {
            _name = name;
            _level = 1;
            _class = classType;

            _equipment = equipment;
            _levelAttribute = baseAttribute;
            _levelUpAttribute = levelUpBonus;
        }

        public double Damage()
        {
            double damageAttribute = TotalAttribute().GetAttribute(GameData.ClassData[_class].DamageAttribute);
            double weaponDamage = _equipment.Weapon?.Damage ?? 1;

            return weaponDamage * (1 + damageAttribute / 100);
        }

        public void Display()
        {
            IAttribute attribute = TotalAttribute();
            StringBuilder output = new();
            output.AppendLine(string.Format(" {0}, Level {1} {2}", Name, Level, GameData.ClassData[_class].ClassName));
            output.AppendLine("  STR: " + attribute.Strength);
            output.AppendLine("  DEX: " + attribute.Dexterity);
            output.AppendLine("  INT: " + attribute.Intelligence);
            output.AppendLine("  DMG: " + Math.Round(Damage(), 2));
            Console.WriteLine(output);
        }

        public void Equip(IItem item)
        {
            _equipment.Equip(item, _level);
        }

        public void LevelUp()
        {
            _level++;
            _levelAttribute = _levelAttribute.Add(_levelUpAttribute);
        }

        public IAttribute TotalAttribute()
        {
            return _levelAttribute.Add(_equipment.Armor.AttributeSum());
        }
    }
}
