﻿using RpgHeroesIndustrialized.Attributes;
using RpgHeroesIndustrialized.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Heroes
{
    public interface IHero
    {
        public string Name { get; }
        public int Level { get; }
        public IAttribute LevelAttribute { get; }
        public IEquipment Equipment { get; }

        public void LevelUp();
        public IAttribute TotalAttribute();
        public double Damage();
        public void Equip(IItem item);
        public void Display();
    }
}
