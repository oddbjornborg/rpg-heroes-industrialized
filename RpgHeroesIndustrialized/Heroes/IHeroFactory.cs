﻿using RpgHeroesIndustrialized.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Heroes
{
    public interface IHeroFactory
    {
        IHero GetHero(string name, HeroClass heroClass);
    }
}
