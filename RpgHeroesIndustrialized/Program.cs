﻿namespace RpgHeroesIndustrialized
{
    using RpgHeroesIndustrialized.Items;
    using RpgHeroesIndustrialized.Enums;
    using RpgHeroesIndustrialized.Heroes;

    internal class Program
    {
        static void Main(string[] args)
        {
            IHero hero = new Hero("Dude", HeroClass.MAGE);
            hero.Display();
        }
    }
}