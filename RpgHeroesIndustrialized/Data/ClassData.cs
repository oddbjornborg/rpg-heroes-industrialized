﻿using RpgHeroesIndustrialized.Enums;
using RpgHeroesIndustrialized.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Data
{
    public static class GameData
    {
        public static IDictionary<HeroClass, ClassStruct> ClassData { get; } = new Dictionary<HeroClass, ClassStruct>()
        {
            {
                HeroClass.MAGE, new(
                    "Mage",
                    AttributeType.INTELLIGENCE,
                    new List<ArmorType>() { ArmorType.CLOTH },
                    new List<WeaponType>() { WeaponType.WAND, WeaponType.STAFF },
                    new HeroAttribute(1, 1, 7),
                    new HeroAttribute(1, 1, 5)
                )
            },
            {
                HeroClass.RANGER, new(
                    "Ranger",
                    AttributeType.DEXTERITY,
                    new List<ArmorType>() { ArmorType.LEATHER, ArmorType.MAIL },
                    new List<WeaponType>() { WeaponType.BOW },
                    new HeroAttribute(1, 7, 1),
                    new HeroAttribute(1, 5, 1)
                )
            },
            {
                HeroClass.ROGUE, new(
                    "Rogue",
                    AttributeType.DEXTERITY,
                    new List<ArmorType>() { ArmorType.LEATHER, ArmorType.MAIL },
                    new List<WeaponType>() { WeaponType.DAGGER, WeaponType.SWORD },
                    new HeroAttribute(2, 6, 1),
                    new HeroAttribute(1, 4, 1)
                )
            },
            {
                HeroClass.WARRIOR, new(
                    "Warrior",
                    AttributeType.STRENGTH,
                    new List<ArmorType>() { ArmorType.MAIL, ArmorType.PLATE },
                    new List<WeaponType>() { WeaponType.SWORD, WeaponType.AXE, WeaponType.HAMMER },
                    new HeroAttribute(5, 2, 1),
                    new HeroAttribute(3, 2, 1)
                )
            }
        };
    }
}
