﻿using RpgHeroesIndustrialized.Attributes;
using RpgHeroesIndustrialized.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Data
{
    public struct ClassStruct
    {
        public string ClassName;
        public AttributeType DamageAttribute;
        public List<ArmorType> ValidArmorTypes;
        public List<WeaponType> ValidWeaponTypes;
        public HeroAttribute BaseAttributes;
        public HeroAttribute LevelUpAttributes;

        public ClassStruct(string className, AttributeType damageStat, List<ArmorType> armorTypes, List<WeaponType> weaponTypes, HeroAttribute baseAttribute, HeroAttribute levelUpAttribute)
        {
            ClassName = className;
            DamageAttribute = damageStat;
            ValidArmorTypes = armorTypes;
            ValidWeaponTypes = weaponTypes;
            BaseAttributes = baseAttribute;
            LevelUpAttributes = levelUpAttribute;
        }
    }
}
