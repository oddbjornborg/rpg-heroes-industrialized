﻿using RpgHeroesIndustrialized.Attributes;
using RpgHeroesIndustrialized.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgHeroesIndustrialized.Extensions
{
    static public class SumExtensions
    {
        static public HeroAttribute AttributeSum(this IEnumerable<IArmor?> source)
        {
            return source.Select(a => ((HeroAttribute?)a?.ArmorAttribute) ?? new HeroAttribute()).Aggregate((sum, next) => sum + next);
        }

        static public IAttribute? AttributeSumNew(this IEnumerable<IArmor?> source)
        {
            List<IAttribute> attributes = source.Where(a => a != null && a.ArmorAttribute != null)
                                                .Select(a => a!.ArmorAttribute)
                                                .ToList()!;
            
            if(attributes.Count > 0)
            {
                return attributes.Aggregate((a, b) => a.Add(b));
            }
            else
            {
                return null;
            }
        }
    }
}
